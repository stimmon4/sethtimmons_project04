﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptsEffects
{
    public EffectTypes type;

    public GameObject endPoint;


    public float duration;
    public float intensity;

    public string name;
}
