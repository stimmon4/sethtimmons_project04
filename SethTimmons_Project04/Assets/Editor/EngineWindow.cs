﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class EngineWindow : EditorWindow
{
    public List<ScriptsEffects> effectsList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    Engine engineScript;

    int movementItem = 0;
    int facingItem = 0;

    Vector2 minimumSize = new Vector2(750f, 350f);

    //setup button skins
    GUIStyle miniRight, miniCenter, miniLeft;

    //Position Variables
    float offsetX = 0f, offsetY = 0f, ELEMENT_HEIGHT = 17f;

    public static void Init()
    {
        EngineWindow window = ((EngineWindow)EngineWindow.GetWindow(typeof(EngineWindow)));
        window.Show();
    }

    void OnFocus()
    {
        //reference the script
        engineScript = GameObject.Find("EngineObject").GetComponent<Engine>();

        //BUTTONS
        miniRight = new GUIStyle(EditorStyles.miniButtonRight);
        miniLeft = new GUIStyle(EditorStyles.miniButtonLeft);
        miniCenter = new GUIStyle(EditorStyles.miniButtonMid);

        //Pull info from the script
        movementList = engineScript.movementList;
        effectsList = engineScript.effectsList;
        facingList = engineScript.facingList;

        movementItem = engineScript.lastEditedMovement;
        facingItem = engineScript.lastEditedFacing;

        //Make sure nothing is null
        if (movementList == null)
            movementList = new List<ScriptMovement>();
        if (effectsList == null)
            effectsList = new List<ScriptsEffects>();
        if (facingList == null)
            facingList = new List<ScriptFacings>();

        //Make sure that there is at least one element in the list
        if (movementList.Count <= 0)
            movementList.Add(new ScriptMovement());
        if (effectsList.Count <= 0)
            effectsList.Add(new ScriptsEffects());
        if (facingList.Count <= 0)
            facingList.Add(new ScriptFacings());
    }

    void OnGUI()
    {
        //built in variable for editor window
        minSize = minimumSize;
        offsetY = 0f;
        MovementGUI();
        EffectsGUI();
        FacingsGUI();
    }

    void MovementGUI()
    {
        offsetX = 0;
        offsetY = 0;
        //sets up rects for showing info
        Rect movementLabel = new Rect(45f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(229, 8);
        Vector2 linePointBottom = new Vector2(229, 195);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(movementLabel, "MOVEMENT WAYPOINTS");

        Rect numberLabel = new Rect(90f, offsetY, 200f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(numberLabel, ((movementItem + 1) + " / " + movementList.Count));
        offsetY += ELEMENT_HEIGHT + 2f;
        //Waypoint Name
        Rect nameLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
        //offsetY += ELEMENT_HEIGHT;
        EditorGUI.LabelField(nameLabel, "Waypoint Name: ");

        Rect nameInput = new Rect(offsetX + 100f, offsetY, 115f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT;
        movementList[movementItem].name = EditorGUI.TextField(nameInput, movementList[movementItem].name);

        //Waypoint type
        /*Rect typeLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
        offsetX += 100f;*/

        Rect typeRect = new Rect(offsetX, offsetY, 220f, ELEMENT_HEIGHT);
        offsetY += ELEMENT_HEIGHT + 2f;
        offsetX = 0f;
        movementList[movementItem].type = (MovementTypes)EditorGUI.EnumPopup(typeRect, movementList[movementItem].type);

        //Specific Display Info
        switch(movementList[movementItem].type)
        {
            case MovementTypes.STRAIGHT:
                Rect startLabel = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 100f;
                EditorGUI.LabelField(startLabel, "End Point: ");

                Rect endRect = new Rect(offsetX, offsetY, 115f, ELEMENT_HEIGHT);
                offsetX = 0f;
                movementList[movementItem].endPoint = (GameObject)EditorGUI.ObjectField(endRect, movementList[movementItem].endPoint, typeof(GameObject), true);

                break;
        }

        //Display Buttons

        //locations for buttons
        Rect prevButtonRect = new Rect(50f, 175f, 40f, ELEMENT_HEIGHT);
        if (movementItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                movementItem--;
            }
        }
        Rect addButtonRect = new Rect(90f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            movementList.Insert(movementItem + 1, new ScriptMovement());
        }
        Rect nextButtonRect = new Rect(130f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next");
            if (movementItem < movementList.Count - 1)
                movementItem++;
            else
            {
                movementList.Add(new ScriptMovement());
                movementItem++;
            }
        }

        /*Rect DoIt = new Rect(60f, 250f, 100f, ELEMENT_HEIGHT);
        //offsetY += ELEMENT_HEIGHT;
        GUI.color = Color.magenta + Color.white;
        EditorGUI.LabelField(DoIt, "↑↑↑ DO IT! ↑↑↑");

        Rect deleteRect = new Rect(60f, 200f, 110f, ELEMENT_HEIGHT);
        GUI.color = Color.green;
        if(GUI.Button(deleteRect, "Click Me!"))
        {
            if(movementItem == movementList.Count - 1)
            {
                movementList.RemoveAt(movementItem);
                movementItem--;
            }
            else if (movementList.Count > 1)
            {
                movementList.RemoveAt(movementItem);
            }
            else
            {
                Debug.Log("Gotcha Noob!!!");
            }
        }*/
        if (movementList.Count > 1)
        {
            Rect deleteRect = new Rect(60f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (movementItem == movementList.Count - 1)
                {
                    movementList.RemoveAt(movementItem);
                    movementItem--;
                }
                else if (movementList.Count > 1)
                {
                    movementList.RemoveAt(movementItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;

        //if(GUI.Button())
    }
    void EffectsGUI()
    {
        //sets up rects for showing info
        Rect effectsLabel = new Rect(545f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY += 10f + ELEMENT_HEIGHT;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(750, 8);
        Vector2 linePointBottom = new Vector2(750, 195);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(effectsLabel, "EFFECTS WAYPOINTS");
    }
    void FacingsGUI()
    {
        offsetX = 0;
        offsetY = 0;
        //sets up rects for showing info
        Rect windowDisplay = new Rect(295f, 10f, 250f, ELEMENT_HEIGHT);
        offsetY = 10f;
        //Setup to draw a line
        Vector2 linePointTop = new Vector2(500, 8);
        Vector2 linePointBottom = new Vector2(500, 195);
        Drawing.DrawLine(linePointTop, linePointBottom, Color.green, 1f, true);

        EditorGUI.LabelField(windowDisplay, "FACINGS WAYPOINTS");

        //facing

        Rect numberLabel = new Rect(340f, 40f, 200f, ELEMENT_HEIGHT);
        EditorGUI.LabelField(numberLabel, ((facingItem + 1) + " / " + facingList.Count));
        offsetY += ELEMENT_HEIGHT + 2f;
        windowDisplay = new Rect(250f, 70f, 100f, ELEMENT_HEIGHT);
        offsetX += 100;
        EditorGUI.LabelField(windowDisplay, "Facing Type: ");

        windowDisplay = new Rect(350f, 70f, 100f, ELEMENT_HEIGHT);
        offsetX = 250f;
        offsetY += ELEMENT_HEIGHT + 2f;
        facingList[facingItem].name = EditorGUI.TextField(windowDisplay, facingList[facingItem].name);

        offsetX = 250f;
        offsetY += ELEMENT_HEIGHT * 2 + 30;
        windowDisplay = new Rect(offsetX, offsetY, 105f, ELEMENT_HEIGHT);
        facingList[facingItem].type = (FacingTypes)EditorGUI.EnumPopup(windowDisplay, facingList[facingItem].type);

        offsetY += ELEMENT_HEIGHT;
        switch(facingList[facingItem].type)
        {
            case FacingTypes.LOOKAT:
                windowDisplay = new Rect(offsetX, offsetY, 100f, ELEMENT_HEIGHT);
                offsetX += 90f;
                EditorGUI.LabelField(windowDisplay, "Look at target");

                windowDisplay = new Rect(offsetX, offsetY, 110f, ELEMENT_HEIGHT);
                offsetX = 275;
                offsetY += ELEMENT_HEIGHT;
                facingList[facingItem].lookTarget = (GameObject)EditorGUI.ObjectField(windowDisplay, facingList[facingItem].lookTarget, typeof(GameObject), true);

                windowDisplay = new Rect(offsetX, offsetY, 125f, ELEMENT_HEIGHT);
                offsetX += 125f;
                EditorGUI.LabelField(windowDisplay, "Route to target over");
                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX += 30f;
                facingList[facingItem].speed = EditorGUI.FloatField(windowDisplay, facingList[facingItem].speed);

                windowDisplay = new Rect(offsetX, offsetY, 30f, ELEMENT_HEIGHT);
                offsetX = 285f;
                offsetY += ELEMENT_HEIGHT;
                EditorGUI.LabelField(windowDisplay, "secs");
            
                break;
        }

        Rect prevButtonRect = new Rect(250f, 175f, 40f, ELEMENT_HEIGHT);
        if (facingItem != 0)
        {
            if (GUI.Button(prevButtonRect, "Prev", miniLeft))
            {
                //Debug.Log("Hit previous");
                facingItem--;
            }
        }
        Rect addButtonRect = new Rect(290f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(addButtonRect, "Add", miniCenter))
        {
            Debug.Log("Hit add");
            facingList.Insert(facingItem + 1, new ScriptFacings());
        }
        Rect nextButtonRect = new Rect(330f, 175f, 40f, ELEMENT_HEIGHT);
        if (GUI.Button(nextButtonRect, "Next", miniRight))
        {
            Debug.Log("Hit next");
            if (facingItem < facingList.Count - 1)
                facingItem++;
            else
            {
                facingList.Add(new ScriptFacings());
                facingItem++;
            }
        }

        /*Rect DoIt = new Rect(60f, 250f, 100f, ELEMENT_HEIGHT);
        //offsetY += ELEMENT_HEIGHT;
        GUI.color = Color.magenta + Color.white;
        EditorGUI.LabelField(DoIt, "↑↑↑ DO IT! ↑↑↑");

        Rect deleteRect = new Rect(60f, 200f, 110f, ELEMENT_HEIGHT);
        GUI.color = Color.green;
        if(GUI.Button(deleteRect, "Click Me!"))
        {
            if(movementItem == movementList.Count - 1)
            {
                movementList.RemoveAt(movementItem);
                movementItem--;
            }
            else if (movementList.Count > 1)
            {
                movementList.RemoveAt(movementItem);
            }
            else
            {
                Debug.Log("Gotcha Noob!!!");
            }
        }*/
        if (facingList.Count > 1)
        {
            Rect deleteRect = new Rect(260f, 200f, 110f, ELEMENT_HEIGHT);
            GUI.color = Color.red;
            if (GUI.Button(deleteRect, "Delete Waypoint"))
            {
                if (facingItem == facingList.Count - 1)
                {
                    facingList.RemoveAt(facingItem);
                    facingItem--;
                }
                else if (facingList.Count > 1)
                {
                    facingList.RemoveAt(facingItem);
                }
                else
                {
                    Debug.Log("Cannot delete only waypoint");
                }
            }
        }

        GUI.color = Color.white;
    }

    void OnLostFocus()
    {
        PushData();
    }

    void PushData()
    {
        engineScript.movementList = movementList;
        engineScript.effectsList = effectsList;
        engineScript.facingList = facingList;
        engineScript.lastEditedMovement = movementItem;
        engineScript.lastEditedFacing = facingItem;
    }
}
