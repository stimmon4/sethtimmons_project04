﻿public enum MovementTypes
{
    WAIT,
    STRAIGHT,
    BEIZIER  
}

public enum FacingTypes
{
    LOOKAT,
    WAIT,
    FREELOOK
}

public enum EffectTypes
{
    SHAKE,
    SPLATTER,
    FADE,
    WAIT
}