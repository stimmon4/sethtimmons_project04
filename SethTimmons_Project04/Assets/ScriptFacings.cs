﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptFacings
{
    public FacingTypes type;

    public GameObject lookTarget;
    public float speed;

    public string name;
}
