﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Engine : MonoBehaviour
{
    public GameObject playerObj;
    public List<ScriptsEffects> effectsList;
    public List<ScriptMovement> movementList;
    public List<ScriptFacings> facingList;

    int currentWaypoint;

    public int lastEditedMovement = 0, lastEditedFacing = 0;

    void Start()
    {
        StartCoroutine("Movement Engine");
        StartCoroutine("Facing Engine");
        playerObj = GameObject.FindWithTag("Player");
    }

    IEnumerator MovementEngine()
    {
        for (int i = 0; i < movementList.Count; i++)
        {
            switch(movementList[i].type)
            {
                case MovementTypes.STRAIGHT:
                    StraightLine()
                    break;
            }
            
        }
    }

    IEnumerator StraightLine(float time, Vector3 endPos)
    {
        float elapsedTime = 0f;
        Vector3 startingPos = playerObj.transform.position;
        while(elapsedTime < time)
        {
            playerObj.transform.position = Vector3.Lerp(startingPos, endPos, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        playerObj.transform.position = endPos;
    }

    IEnumerator LookAtTarget(float time, Vector3 lookPosition)
    {
        float elapsedTime = 0f;
        Quaternion startRotation = playerObj.transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation((lookPosition - playerObj.transform.position).normalized);

        while (elapsedTime < time)
        {
            playerObj.transform.rotation = Quaternion.Lerp(startRotation, targetRotation, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        elapsedTime = 0;
        while (elapsedTime < time)
        {
            playerObj.transform.rotation = Quaternion.Lerp(targetRotation, Quaternion.Euler(playerObj.transform.forward), (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        playerObj.transform.rotation = Quaternion.Euler(playerObj.transform.forward);
    }

    IEnumerator FacingEngine()
    {

    }
}
