﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptMovement
{
    public MovementTypes type;

    public GameObject endPoint;
    public float speed;

    public string name;
}
